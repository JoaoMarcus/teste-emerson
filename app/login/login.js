'use strict';

angular.module('myApp.login', ['ngRoute','firebase'])

.config(['$routeProvider', function($routeProvider) {
  $routeProvider.when('/login', {
    templateUrl: 'login/login.html',
    controller: 'LoginCtrl'
  });
}])

.controller('LoginCtrl', ['$scope', function($scope, $firebaseAuth, $rootScope, $location) {

    var config = {
      apiKey: "AIzaSyB88KnSK_FW1llS1s4KbM3SdmASKacKNMs",
      authDomain: "fir-application-9d853.firebaseapp.com",
      databaseURL: "https://fir-application-9d853.firebaseio.com",
      storageBucket: "fir-application-9d853.appspot.com",
    };

      firebase.initializeApp(config);

      var rootRef = firebase.database().ref();

      $scope.user = {};
        $scope.SignIn = function(e){

          var email = $scope.user.email;
          var password = $scope.user.password;
          e.preventDefault();

            if(!email || !password) {
              return console.log('email and password required!');
            }

            firebase.auth().signInWithEmailAndPassword(email, password)
                      .catch(function(error) {
                        var errorCode = error.code;
                        var errorMessage = error.message;
                        var user = firebase.auth().currentUser;
                      });
  
          }

      firebase.auth().onAuthStateChanged(function(user) {
                    if (user) {
                      console.log(user);
                      logout.classList.remove('hide');
                    } else {
                      console.log('Sorry you were not logged in.');
                      logout.classList.add('hide');
                    }
      });

      $scope.SignOut = function(e) {
        firebase.auth().signOut();
      }

      $scope.clickOn = function($location) {
        $location.path('signup.html');
      }
 
      $scope.SignUp = function() {

        var name = $scope.user.name;
        var email = $scope.user.email;
        var password = $scope.user.password;

            if(!email || !password) {
              return console.log('email and password required!');
            }

            firebase.auth().createUserWithEmailAndPassword(email, password)
            .catch(function(error) {
              console.log('register error', error);
              if (error.code === 'auth/email-already-in-use') {
                var credential = firebase.auth.EmailAuthProvider.credential(email, password);
                app.signInWithGoogle()
                  .then(function() {
                    firebase.auth().currentUser.link(credential)
                      .then(function(user) {
                        console.log("Account linking success", user);
                      }, function(error) {
                        console.log("Account linking error", error);
                      });
                  });
              }
            });
      }
}]);
