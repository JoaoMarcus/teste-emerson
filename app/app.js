'use strict';

// Declare app level module which depends on views, and components


var app = angular.module('myApp', ['ngRoute']);

app.config(function($routeProvider, $locationProvider){
	$locationProvider.html5Mode(true);
	$routeProvider.when('/', {
		templateUrl: 'app/login/login.html',
		controller: 'LoginCtrl',
	})
	.when('/signup', {
		templateUrl: 'app/login/signup.html',
		controller: 'SignUpCtrl',
	})

	.otherwise({ redirectTo: '/login' });
});